;;; client2.el ---   -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar my-proc-2 nil)

(when (processp my-proc-2)
  (delete-process my-proc-2)
  (setq my-proc-2 nil))

(setq my-proc-2 (make-network-process :name "*my-example-2*"
                                      :buffer "*my-example-2*"
                                      :filter #'my-filter-2
                                      :host 'local
                                      :service 8080))

(defun my-filter-2 (proc data)
  "Just print the message."
  (message data))

(process-send-string
 my-proc-2
 (lsp--make-message '((jsonrpc . "2.0")
                      (message . "ラウトは難しいです！"))))

(process-send-string
 my-proc-2
 (lsp--make-message '((jsonrpc . "2.0")
                      (message . "something else！"))))

;;; client2.el ends here
