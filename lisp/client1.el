;;; client1.el ---   -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar my-proc-1 nil)

(when (processp my-proc-1)
  (delete-process my-proc-1)
  (setq my-proc-1 nil))

(setq my-proc-1 (make-network-process :name "*my-example-1*"
                                      :buffer "*my-example-1*"
                                      :filter #'my-filter-1
                                      :host 'local
                                      :service 8080))

(defun my-filter-1 (proc data)
  "Just print the message."
  (message data))

(defun my-send (obj)
  "Send message to the server."
  (process-send-string my-proc-1
                       (lsp--make-message obj)))

(my-send '((jsonrpc . "2.0")
           (method . "some")
           (message . "ラウトは難しいです！")))

(my-send '((jsonrpc . "2.0")
           (method . "login")
           (message . "something else！")))

;;; client1.el ends here
