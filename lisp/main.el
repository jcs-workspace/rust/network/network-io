;;; main.el ---   -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar my-proc-1 nil)
(defvar my-proc-2 nil)

(when (processp my-proc-1)
  (delete-process my-proc-1)
  (setq my-proc-1 nil))

(when (processp my-proc-2)
  (delete-process my-proc-2)
  (setq my-proc-2 nil))

(setq my-proc-1 (make-network-process :name "*my-example-1*"
                                      :buffer "*my-example-1*"
                                      :filter #'my-filter
                                      :host 'local
                                      :service 8080))

(setq my-proc-2 (make-network-process :name "*my-example-2*"
                                      :buffer "*my-example-2*"
                                      :filter #'my-filter
                                      :host 'local
                                      :service 8080))

(defun my-filter (proc data)
  "Just print the message."
  (message data))

(process-send-string my-proc-1
                     "0123456789ABCDEFWWW")

(process-send-string my-proc-1
                     "aaaaaaaaaaaaaaaaaaaa")

(process-send-string my-proc-2
                     "ラウトは難しいです！")

;;; main.el ends here
