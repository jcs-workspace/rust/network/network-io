use log::Level;
use tokio::{io::AsyncReadExt, time};

fn fib(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1,
        n => fib(n - 1) + fib(n - 2),
    }
}

async fn sleeper(name: &str) {
    log::info!("{}: Sleeping", name);
    time::sleep(time::Duration::from_secs(1)).await;
    log::info!("Awake!");
}

async fn reader() {
    log::info!("Reading some beeg data");
    let mut f = tokio::fs::File::open("movies.csv").await.unwrap();
    let mut contents = vec![];
    f.read_to_end(&mut contents).await.unwrap();
    log::info!("Read beeg {} bytes", contents.len());

    // Expensive on CPU
    fib(40);
}

async fn run() {
    tokio::spawn(async {
        sleeper("first").await;
        sleeper("second").await;
    });
    reader().await;
}

#[allow(dead_code)]
pub async fn execute() {
    simple_logger::init_with_level(Level::Info).unwrap();

    //let rt = tokio::runtime::Runtime::new().unwrap();
    //let future = run();

    let start = std::time::Instant::now();
    run().await;
    let end = std::time::Instant::now();

    print!("Took {:?} seconds", end - start);
}
