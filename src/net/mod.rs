/**
 * $File: mod.rs $
 * $Date: 2024-04-22 00:03:46 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
pub mod packet;
