use std::io;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;
use tracing_subscriber::{fmt, layer::SubscriberExt};

use crate::{net, packet_handler};

const SEPARATOR_LEN: usize = "\r\n".len();
const BUF_SIZE: usize = 1024;

fn get_content_len(line: &str) -> usize {
    if !line.starts_with("Content-Length: ") {
        tracing::error!("Invalid content length: {:?}", line);
        return 0;
    }
    let rm_len = "Content-Length: ".len();
    let len_str = &line[rm_len..];
    len_str.parse::<usize>().unwrap()
}

struct Connection {
    pub socket: tokio::net::TcpStream,
    pub addr: std::net::SocketAddr,
    read_buf: [u8; BUF_SIZE],
    data: Vec<u8>,
    packets: Vec<net::packet::Packet>,
}

impl Connection {
    pub fn new(_socket: tokio::net::TcpStream, _addr: std::net::SocketAddr) -> Self {
        let connection = Self {
            socket: _socket,
            addr: _addr,
            read_buf: [0; BUF_SIZE],
            data: Vec::new(),
            packets: Vec::new(),
        };
        connection
    }

    pub async fn start(&mut self) {
        // In a loop, read data from the socket and write the data back.
        loop {
            self.read().await;
            // Write the data back
            //self.write(self.read_buf).await;
        }
    }

    pub async fn read(&mut self) {
        let _ = match self.socket.read(&mut self.read_buf).await {
            // socket closed
            Ok(n) if n == 0 => return,
            Ok(n) => {
                tracing::trace!("{} ({:?})", self.to_string(), n);

                // Add new data to the end of data buffer.
                {
                    let new_data = &self.read_buf[0..n];
                    self.data.append(&mut new_data.to_vec());
                }

                self.process();

                n
            }
            Err(e) => {
                println!("failed to read from socket; err = {:?}", e);
                return;
            }
        };
    }

    pub fn process(&mut self) {
        let decrypted = String::from_utf8_lossy(&self.data);

        let chopped = decrypted.split("\r\n");
        let size = chopped.clone().count();

        if size < 3 {
            return;
        }

        let mut content_len: usize = 0;
        let mut op = 0;
        let mut boundary = 0;
        let mut process = false;

        for line in chopped {
            let current_op = op % 3;

            match current_op {
                0 => {
                    boundary += line.len() + SEPARATOR_LEN;
                    content_len = get_content_len(line);
                }
                1 => {
                    boundary += line.len() + SEPARATOR_LEN;
                }
                2 => {
                    if content_len <= line.len() {
                        boundary += content_len;

                        let data = &line[..content_len];
                        packet_handler::handle(data);
                        //println!("{}: {}", "receive all", data);

                        process = true;
                        break;
                    }
                }
                _ => {
                    tracing::error!("Invalid operation id: {:?}", current_op);
                }
            }
            op += 1;
        }

        if process {
            self.data = self.data[boundary..].to_vec();
            tracing::trace!(
                "data left ({}) {:?}",
                boundary,
                String::from_utf8_lossy(&self.data)
            );
            self.process();
        }
    }

    pub async fn write(&mut self, buf: [u8; BUF_SIZE]) {
        if let Err(e) = self.socket.write_all(&buf).await {
            tracing::warn!("failed to write to socket {:?}; err = {:?}", self.socket, e);
            return;
        }
    }

    pub fn to_string(&self) -> String {
        format!("{}", &self.addr)
    }
}

struct Server {
    host: String,
    port: u16,
}

impl Server {
    pub fn new(_host: String, _port: u16) -> Self {
        Self {
            host: _host,
            port: _port,
        }
    }

    fn addr(&mut self) -> String {
        self.host.to_string() + ":" + &self.port.to_string()
    }

    pub async fn start(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        tracing::info!("Listening on port {}", self.addr());

        let listener = TcpListener::bind(self.addr()).await?;

        loop {
            let (socket, addr) = listener.accept().await?;
            let mut connection = Connection::new(socket, addr);
            tracing::info!("New connection from {}", connection.to_string());

            tokio::spawn(async move {
                connection.start().await;
            });
        }
    }
}

/// Setup logger rotator.
///
/// https://docs.rs/tracing-appender/0.2.3/tracing_appender/non_blocking/struct.WorkerGuard.html
pub fn setup_logger() -> tracing_appender::non_blocking::WorkerGuard {
    let file_appender = tracing_appender::rolling::hourly("./.log", "example.log");
    let (non_blocking, guard) = tracing_appender::non_blocking(file_appender);

    let subscriber = tracing_subscriber::registry()
        .with(fmt::Layer::new().with_writer(io::stdout))
        .with(fmt::Layer::new().with_writer(non_blocking));
    tracing::subscriber::set_global_default(subscriber).expect("Unable to set a global subscriber");
    guard // Don't drop this!
}

pub async fn execute() {
    let _guard = setup_logger();
    let mut server = Server::new("127.0.0.1".to_string(), 8080);
    let _ = server.start().await;
}
